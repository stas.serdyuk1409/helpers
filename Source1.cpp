﻿// Animal.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include <iostream>
class Animal
{
public:
	virtual void makeSound() const = 0;
};

class Cat : public Animal
{
public:
	void makeSound() const override
	{
		std::cout << "Meow\n";
	}
};

class Dog : public Animal
{
public:
	void makeSound() const override
	{
		std::cout << "Woof\n";
	}
};

class Cow : public Animal
{
public:
	void makeSound() const override
	{
		std::cout << "Myyy\n";
	}
};



int main()
{
	Animal* animals[3];
	animals[0] = new Cat();
	animals[1] = new Dog();
	animals[2] = new Cow();

	for (Animal* a : animals)
		a->makeSound();
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
