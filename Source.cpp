﻿#include "Lesson.h"
#include <iostream>
#include <string>

template<typename T>

class Stack
{
private:
	int NumArray = 0;
	T* StackArray = new T[NumArray];


public:
	Stack()
	{

	}
	//Добавляет элемент
	void Push(T Value)
	{
		T* NewArray = new T[NumArray + 1];
		std::cout << "Добавлен элемент " << Value << endl;

		for (int i = 0; i < NumArray; i++)
		{
			NewArray[i] = StackArray[i];
		}
		NewArray[NumArray] = Value;
		delete[] StackArray;

		NumArray++;
		StackArray = NewArray;
	}
	//Удаляет последний элемент
	void Del()
	{
		std::cout << "Удалён последний элемент " << endl;
		T* NewArray = new T[NumArray - 1];
		for (int i = 0; i < NumArray - 1; i++)
		{
			NewArray[i] = StackArray[i];;
		}
		delete[] StackArray;
		NumArray--;
		StackArray = NewArray;
	}

	//Достать верхний элемент из стека
	T Pop()
	{
		return StackArray[NumArray - 1];
	}

	//Печатает массив
	void Print()
	{
		for (int i = 0; i < NumArray; i++)
		{

			std::cout << *(StackArray + i) << "   ";
		}

		std::cout << endl;
	}
	//Чистим память
	~Stack()
	{
		delete[] StackArray;
	}

};